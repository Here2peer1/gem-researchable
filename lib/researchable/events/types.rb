module Researchable
    module Events
        module Types
            REGISTER_POLAR_USER = 'REGISTER_POLAR_USER'
        end

        module Serialializer
            def new_register_polar_user(member_id:)
                { event_type: ::Types::REGISTER_POLAR_USER,
                  member_id: member_id }.to_json
            end
        end

        module Deserializer
            def register_polar_user(message)
                event = message.raw_content
                raise InvalidArgumentException.new "register_polar_user message is invalid" unless valid_register_polar?(event)
                event
            end

            def valid_register_polar?(hash)
                hash.has_key? "member_id"
            end
        end
    end
end