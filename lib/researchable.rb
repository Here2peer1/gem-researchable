require 'researchable/messaging'
require 'researchable/protobuf'
require 'researchable/rabbit_connection'
require 'researchable/responses'
require 'researchable/version'

module Researchable
  class Error < StandardError; end
  # Your code goes here...
end
